'use strict';

const express = require('express'),
      bodyParser = require('body-parser'),
      cors = require('cors'),
      app = express(),
      db = require('./server'),
      port = 8000 ;

      app.use('*', cors())
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());


app.get('/', (request, response) => {
    response.json({ info: 'Hello from the Node JS RESTful side!' })
  });

app.get('/users', db.getUsers)
app.get('/users/:id', db.findUsers)
app.post('/users', db.createUsers)
app.put('/users/:id', db.updateUsers)
app.delete('/users/:id', db.deleteUsers)

app.get('/palindrome/:id', function(req,res) {
  var value = req.params.id;
  
  var palindrome = (str) => {
      var len = str.length;
      var mid = Math.floor(len/2);
      for ( var i = 0; i < mid; i++ ) {
          if (str[i] !== str[len - 1 - i]) {
              return false;
          }
      }
      return true;
  }
  var result = palindrome(value);
  return res.status(200).send({
      status: 'OK',
      value: result
  });
})


app.listen(port);
console.log('Hello from the Node JS RESTful side!"' + port);
