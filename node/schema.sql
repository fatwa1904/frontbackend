--
-- PostgreSQL database dump
--

-- Dumped from database version 11.3 (Debian 11.3-1.pgdg90+1)
-- Dumped by pg_dump version 11.3 (Debian 11.3-1.pgdg90+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: project1; Type: SCHEMA; Schema: -; Owner: project1
--

CREATE SCHEMA project1;

ALTER SCHEMA project1 OWNER TO project1;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: Produk; Type: TABLE; Schema: project1; Owner: project1
--

CREATE TABLE project1."Produk" (
    id integer NOT NULL,
    nama character varying(30) NOT NULL,
    harga double precision DEFAULT '0'::double precision NOT NULL
);


ALTER TABLE project1."Produk" OWNER TO project1;

--
-- Name: TABLE "Produk"; Type: COMMENT; Schema: project1; Owner: project1
--

COMMENT ON TABLE project1."Produk" IS 'Produk stores all the information required for login';


--
-- Name: Produk_id_seq; Type: SEQUENCE; Schema: project1; Owner: project1
--

CREATE SEQUENCE project1."Produk_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE project1."Produk_id_seq" OWNER TO project1;

--
-- Name: Produk_id_seq; Type: SEQUENCE OWNED BY; Schema: project1; Owner: project1
--

ALTER SEQUENCE project1."Produk_id_seq" OWNED BY project1."Produk".id;


--
-- Name: Produk id; Type: DEFAULT; Schema: project1; Owner: project1
--

ALTER TABLE ONLY project1."Produk" ALTER COLUMN id SET DEFAULT nextval('project1."Produk_id_seq"'::regclass);


--
-- Name: Produk Produk_pkey; Type: CONSTRAINT; Schema: project1; Owner: project1
--

ALTER TABLE ONLY project1."Produk"
    ADD CONSTRAINT "Produk_pkey" PRIMARY KEY (id);
	
--
-- Data for Name: Produk; Type: TABLE DATA; Schema: project1; Owner: project1
--

INSERT INTO project1."Produk" (id, nama, harga) VALUES (1, 'HP Samsung', 10000000);

--
-- PostgreSQL database dump complete
--